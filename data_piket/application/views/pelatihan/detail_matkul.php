<!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="<?php echo base_url('assets/img/logo.png');?>" alt="picture">
                  <h3 class="profile-username text-center"><?php echo $matkul['nama_matkul']; ?></h3>
                  <p class="text-muted text-center">Waktu Pelatihan : 18.30 - 20.00 WIB</p>

                  <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                      <b>Peserta</b> <a class="pull-right"><?php echo $jml_peserta; ?></a>
                    </li>
                  </ul>
                  <a id="modal-570109" href="#modal-container-570109" role="button" class="btn btn-primary btn-block" data-toggle="modal"><b>JOIN !!!</b></a>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

              <div class="modal fade" id="modal-container-570109" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                     
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                    <i class="glyphicon glyphicon-remove"></i>
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">
                                        Gabung Pelatihan 
                                    </h4>
                                </div>
                                <div class="modal-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <form class="form-horizontal" role="form" method="POST" action="<?php echo site_url('pelatihan/add_peserta');?>">
                                          <input name="id_pelatihan" type="hidden" value="<?php echo $matkul['id_pelatihan']; ?>">
                                            <div class="form-group">
                                                <label for="inputNama" class="col-sm-2 control-label">
                                                    <h5>Nama</h5>
                                                </label>
                                                <div class="col-sm-10">
                                                    <input name="nama" class="form-control" id="inputNama" required/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputAlasan" class="col-sm-2 control-label">
                                                    <h5>Alasan Gabung</h5>
                                                </label>
                                                <div class="col-sm-10">
                                                    <input name="alasan" class="form-control" id="inputAlasan"/>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="inputUsulan" class="col-sm-2 control-label">
                                                    <h5>Usulan Hari</h5>
                                                </label>
                                                <div class="col-sm-10">
                                                    <input name="usulan" class="form-control" id="inputUsulan"/>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                     
                                                <button type="button" class="btn btn-default" data-dismiss="modal">
                                                    Close
                                                </button> 
                                                <button type="submit" class="btn btn-primary">
                                                    Submit
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                </div>
                                
                            </div>
                        </div>
                        </div> <!-- END MODAL -->

              <!-- About Me Box -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Info</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <strong><i class="fa fa-book margin-r-5"></i>Sekilas Materi</strong>
                  <p class="text-muted">
                    <?php echo $matkul['deskripsi']; ?>
                  </p>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Daftar Peserta</h3>
              </div>
              <div class="col-md-12">
                <div class="callout callout-info">Untuk mengubah atau membatalkan pelatihan silakan menghubungi pihak <b>PSDM DISPLAY!</b></div>
              </div>

              <div class="col-md-12">
                <div><?php echo $this->sys_notif->display(); ?></div>
              </div>
              
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Nama Mata Kuliah</th>
                        <th>Alasan gabung</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $no=1; foreach($peserta as $d){ ?>
                    <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $d['nama_peserta']; ?></td>
                    <td><?php echo $d['alasan']; ?></td>
                    </tr>
                      <?php } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No.</th>
                        <th>Nama Mata Kuliah</th>
                        <th>Alasan gabung</th>
                      </tr>
                    </tfoot>
                  </table>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->