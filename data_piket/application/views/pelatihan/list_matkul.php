          <!-- Main content -->
          <section class="content">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">List Mata Kuliah</h3>
              </div>
              <div class="col-md-12">
                <div class="callout callout-info">Untuk mengubah atau membatalkan pelatihan silakan menghubungi pihak <b>PSDM DISPLAY!</b></div>
              </div>

              <div class="col-md-12">
                <div><?php echo $this->sys_notif->display(); ?></div>
              </div>
              <div class="box-body">
                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Nama Mata Kuliah</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php $no=1; foreach($data as $d){ ?>
                    <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $d['nama_matkul']; ?></td>
                    <td><a href="<?php  echo site_url('pelatihan/view_detail/'.$d['id_pelatihan']);?>" class="btn btn-warning">Detail</a></td>
                    </tr>
                      <?php } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No.</th>
                        <th>Nama Mata Kuliah</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->