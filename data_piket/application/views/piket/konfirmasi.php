          <!-- Main content -->
          <section class="content">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Konfirmasi Piket</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form method="POST" action="<?php echo site_url('piket/confirm') ;?>">
                  <div class="box-body">
                      <input type="hidden" class="form-control" name="id" value="<?php echo $id; ?>">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Lengkap</label>
                      <input type="text" class="form-control" name="nama">
                    </div>  
                    
                      <input type="hidden" class="form-control" name="hari" value="<?php echo $hari; ?>">
                      <input type="hidden" class="form-control" name="jam" value="<?php echo $jam; ?>">
                    </div><!-- /.box-body -->

                  <div class="box-footer">
                    <a href="<?php echo site_url('piket') ?>"><button type="button" class="btn btn-default">Cancel</button></a>
                    <button type="submit" class="btn btn-info pull-right" onclick="doconfirm();">Confirm</button>
                  </div><!-- /.box-footer -->
                </form>
              </div><!-- /.box -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!--/.col (left) -->
                </form>
              </div><!-- /.box -->
                  </form>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!--/.col (right) -->
          </div>   <!-- /.row -->
        </section><!-- /.content -->