          <!-- Main content -->
          <section class="content">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Jadwal Piket</h3>
              </div>
              <div class="col-md-12">
                <div class="callout callout-info">Untuk mengubah atau membatalkan Jadwal Piket silakan menghubungi pihak <b>PSDM DISPLAY!</b></div>
              </div>

              <div class="col-md-12">
                <div><?php echo $this->sys_notif->display(); ?></div>
              </div>
              <div class="box-body">
                <form method="GET" action="<?php echo site_url('piket') ;?>">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="inputHari" class="col-sm-2 control-label">
                          <h5>Pilih Hari</h5>
                      </label>
                      <div class="col-sm-3">
                          <select name="hari" class="form-control" id="inputHari">
                            <option <?php if($hari=='Senin')echo "selected";?> >Senin</option>
                            <option <?php if($hari=='Selasa')echo "selected";?> >Selasa</option>
                            <option <?php if($hari=='Rabu')echo "selected";?> >Rabu</option>
                            <option <?php if($hari=='Kamis')echo "selected";?> >Kamis</option>
                            <option <?php if($hari=='Jumat')echo "selected";?> >Jumat</option>
                            <option <?php if($hari=='Semua Hari')echo "selected";?> >Semua Hari</option>
                          </select>
                      </div>
                      <button type="submit" class="btn btn-default">Lihat</button>
                  </div> 
                    </div><!-- /.box-body -->
                </form>

                <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Hari</th>
                        <th>Jam</th>
                        <th>Nama</th>
                        <!-- <th>Action</th> -->
                      </tr>
                    </thead>
                    <tbody>
                    <?php $no=1; foreach($piket as $d){ ?>
                    <tr>
                    <td><?php echo $no++; ?></td>
                    <td><?php echo $d['hari']; ?></td>
                    <td><?php echo $d['jam']; ?></td>
                    <td><?php echo $d['nama']; ?></td>
                   <!--  <td><a href=" -->
                      <?php // echo site_url('piket/do_delete/'.$d['id']);?>
                      <!-- " class="btn btn-danger" onClick="return doconfirm();">Delete</a></td> -->
                    </tr>
                      <?php } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No.</th>
                        <th>Hari</th>
                        <th>Jam</th>
                        <th>Nama</th>
                        <!-- <th>Action</th> -->
                      </tr>
                    </tfoot>
                  </table>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->