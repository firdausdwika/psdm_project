          <!-- Main content -->
          <section class="content">
            <div class="box box-default">
              <div class="box-header with-border">
                <h3 class="box-title">Jadwal Piket</h3>
              </div>
              <div><?php echo $this->sys_notif->display(); ?></div>
              <div class="box-body">
              <!-- form start -->
                <form method="POST" action="<?php echo site_url('piket/do_insert') ;?>">
                  <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Hari</th>
                        <th>Jam</th>
                        <th>Kuota</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php foreach($data as $d){ ?>
                    <tr>
                    <td><?php echo $d['id']; ?></td>
                    <td><?php echo $d['hari']; ?></td>
                    <td><?php echo $d['jam']; ?></td>
                    <td><?php echo $d['kuota'].'/8'; ?></td>
                    <td>
                      <?php if($d['kuota'] >= 8) {
                        echo '<a href="#" class="btn btn-default">+</a>';
                      }else {?>
                      <a href="<?php echo site_url('piket/edit/'.$d['id']);?>" class="btn btn-info" >+</a>
                      <?php } ?>

                    </td>
                    </tr>
                      <?php } ?>
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>No.</th>
                        <th>Hari</th>
                        <th>Jam</th>
                        <th>Kuota</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
                  </table>
                  </div><!-- /.box-body -->
                </form>
              </div><!-- /.box -->
                </div><!-- /.box-body -->
            </div><!-- /.box -->
          </section><!-- /.content -->