<?php
class Sys_notif extends CI_Model {

    function __construct(){
        parent::__construct();
        $temp =  $this->session->userdata("sys_notif");
        if(empty($temp)){
        	$this->reset();
        }
    }
    function reset(){
    	$this->session->set_userdata("sys_notif", array("danger" => array(), "warning" => array(), "success" => array()));
    }
    function add($type, $msg){
		$temp = $this->session->userdata("sys_notif");
    	$temp[$type][] = $msg;
    	$this->session->set_userdata("sys_notif", $temp);
    }
    function success($msg){ $this->add("success", $msg); }
    function warning($msg){ $this->add("warning", $msg); }
    function danger($msg){ $this->add("danger", $msg); }

    function display(){
    	$temp = $this->session->userdata("sys_notif");
    	$html = '';
    	foreach ($temp as $type => $value) {
    		foreach ($value as $msg) {
    			$html .= '<div class="alert alert-'. $type .'" role="alert">'. $msg .'</div>' . "\n";
    		}
    	}
    	$this->reset();
    	return $html;
    }
    function countDanger(){
        $temp = $this->session->userdata("sys_notif");
        return count($temp["danger"]);
    }
}
?>