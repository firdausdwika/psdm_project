<?php
class M_Piket extends CI_Model{
    
    function insertData($namatabel,$data){
   		$current = $this->db->insert($namatabel,$data);
   		return $current;
    }
    public function getData($where,$order)
    {
        $data = $this->db->query('select * from data_piket '.$where.' '.$order);
        return $data->result_array();
    }

    public function ambilData()
    {
        $data = $this->db->query('select * from jadwal_piket');
        return $data->result_array();
    }

    public function editData($where="")
    {
        $data = $this->db->query('select * from jadwal_piket'.$where);
        return $data->result_array();
    }

    public function updateData($namatabel,$data,$where)
    {
        $data = $this->db->update($namatabel,$data,$where);
        return $data;
    }

    public function deleteData($namatabel,$where)
    {
        $data = $this->db->delete($namatabel,$where);
        return $data;
    }

    public function get_kuota($id){
        $sql = "SELECT kuota FROM jadwal_piket WHERE id = '$id'";
        $query = $this -> db -> query($sql);
        $temp = $query -> row_array();
        if (empty($temp["kuota"]))
            return false;
        return $temp['kuota'];
    }

    public function get_id_jadwal($id){
        $sql = "SELECT id_jadwal FROM data_piket WHERE id = '$id'";
        $query = $this -> db -> query($sql);
        $temp = $query -> row_array();
        if (empty($temp["id_jadwal"]))
            return false;
        return $temp['id_jadwal'];
    }   
}