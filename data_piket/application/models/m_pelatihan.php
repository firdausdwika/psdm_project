<?php
class M_Pelatihan extends CI_Model{
    
    function insertData($namatabel,$data){
   		$current = $this->db->insert($namatabel,$data);
   		return $current;
    }
    public function getAllDataPelatihan()
    {
        $data = $this->db->query('select * from pelatihan_matkul');
        return $data->result_array();
    }

    public function ambilData($nama_tabel,$where="")
    {
        $data = $this->db->query('select * from '.$nama_tabel.' '.$where);
        return $data;//->result_array();
    }

    public function ambilJumlahPeserta($where="")
    {
        $data = $this->db->query('select count(*) jumlah from peserta_pelatihan'.$where);
        $temp = $data->row_array();
        return $temp['jumlah'];

    }

}