<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Piket extends CI_Controller {

	function index()
	{
		date_default_timezone_set('Africa/Lagos');
		$lihat_hari='';
		if(!empty($_GET['hari'])){
			$lihat_hari=$_GET['hari'];
		}else{
			$today =date("w");
			$lihat_hari = $this->getHari($today);
		}

		$where='';
		if($lihat_hari != 'Semua Hari'){
			$where="where hari = '$lihat_hari'";
		}
		$order='order by jam';
		$data['piket'] = $this->m_piket->getData($where,$order);
		$data['hari']=$lihat_hari;

		$header['title']='Data Piket || DISPLAY';
		$header['halaman']='Data Piket';
		$this->load->view('include/header',$header);
		$this->load->view('piket/home',$data);
		$this->load->view('include/footer');
	}

	function pilih()
	{
		$data = $this->m_piket->ambilData();
		$header['title']='Data Piket || DISPLAY';
		$header['halaman']='Data Piket';
		$this->load->view('include/header',$header);
		$this->load->view('piket/add_piket',array('data' => $data));
		$this->load->view('include/footer');
	}

	public function edit($id)
	{
		$res = $this->m_piket->editData(" where id = $id");
		$data = array(
			"id" => $res[0]['id'],
			"hari" => $res[0]['hari'],
			"jam" => $res[0]['jam'],
			);
		$header['title']='Data Piket DISPLAY';
		$header['halaman']='Data Piket';
		$this->load->view('include/header',$header);
		$this->load->view('piket/konfirmasi',$data);
		$this->load->view('include/footer');
	}	

	public function confirm()
	{
		$id = $_POST['id'];
		$nama = $_POST['nama'];
		$hari = $_POST['hari'];
		$jam = $_POST['jam'];
		$data = array(
			'nama' => $nama,
			'hari' => $hari,
			'jam' => $jam,
			'id_jadwal' => $id, 
		);
		$kuota = $this->m_piket->get_kuota($id);
		if($kuota < 8){
			$res = $this->m_piket->insertData('data_piket',$data);
			$data2=array('kuota' => 1+$kuota);
			$where='id = '.$id;
			$res2 = $this->m_piket->updateData('jadwal_piket',$data2,$where);
			$this->sys_notif->success("Jadwal berhasil ditambahkan");
			redirect('piket?hari='.$hari);
		}else{
			$this->sys_notif->warning("Jadwal telah memenuhi kuota!");
			redirect('piket/pilih');
		}
		
	}

	public function do_delete($id)
	{
		$where = array('id' => $id);
		$id_jadwal = $kuota = $this->m_piket->get_id_jadwal($id);

		$kuota = $this->m_piket->get_kuota($id_jadwal)-1;
		$where2= array('id' => $id_jadwal);
		$data2=array('kuota' => $kuota);

		$res = $this->m_piket->deleteData('data_piket',$where);

		$res2 = $this->m_piket->updateData('jadwal_piket',$data2,$where2);
		$this->sys_notif->success("Jadwal telah dihapus");
		redirect('piket');	
	}

	function getHari($id_hari){
		$hari='';
		switch ($id_hari) {
			case 1:
				$hari='Senin';
				break;
			case 2:
				$hari='Selasa';
				break;
			case 3:
				$hari='Rabu';
				break;
			case 4:
				$hari='Kamis';
				break;
			case 5:
				$hari='Jumat';
				break;
			
			default:
				$hari='Senin';
				break;
		}
		return $hari;
	}	
}