<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pelatihan extends CI_Controller {

	function index()
	{
		$header['title']='Pelatihan Mata Kuliah || DISPLAY';
		$header['halaman']='Pelatihan';
		$data = $this->m_pelatihan->getAllDataPelatihan();
		$this->load->view('include/header',$header);
		$this->load->view('pelatihan/list_matkul',array('data' => $data));
		$this->load->view('include/footer');
	}

	public function view_detail($id)
	{
		$temp = $this->m_pelatihan->ambilData("pelatihan_matkul","where id_pelatihan = $id");
		$data['matkul'] = $temp->row_array();
		$temp2 = $this->m_pelatihan->ambilData("peserta_pelatihan","where id_pelatihan = $id");
		$data['peserta'] = $temp2->result_array();
		$data['jml_peserta'] = $this->m_pelatihan->ambilJumlahPeserta(" where id_pelatihan = $id");

		$header['title']='Pelatihan Mata Kuliah ||DISPLAY';
		$header['halaman']='Pelatihan';
		$this->load->view('include/header',$header);
		$this->load->view('pelatihan/detail_matkul',$data);
		$this->load->view('include/footer');
	}

	public function add_peserta(){
		$id_pelatihan = $_POST['id_pelatihan'];
		$nama = $_POST['nama'];
		$alasan = $_POST['alasan'];
		$usulan = $_POST['usulan'];
		$data = array(
			'id_pelatihan' => $id_pelatihan,
			'nama_peserta' => $nama,
			'alasan' => $alasan,
			'usulan' => $usulan,
		);
		$res = $this->m_pelatihan->insertData('peserta_pelatihan',$data);
		$this->sys_notif->success("Selamat Anda Berhasil Bergabung");
		redirect('pelatihan/view_detail/'.$id_pelatihan);

	}


}