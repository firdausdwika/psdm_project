package display.absensi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NewProductActivity extends ActionBarActivity {

    // Progress Dialog
    private ProgressDialog progressDialog;

    JSONParser jsonParser = new JSONParser();
    EditText inputNama;
    EditText inputDivisi;
    EditText inputKeterangan;

    // url to create new product
    private static String url_create_product = "http://display.ub.ac.id/absensi/create_product.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_product);

        // Edit Text
        inputNama = (EditText) findViewById(R.id.inputNama);
        inputDivisi = (EditText) findViewById(R.id.inputDivisi);
        inputKeterangan = (EditText) findViewById(R.id.inputKeterangan);

        // Create button
        Button bAbsen = (Button) findViewById(R.id.bAbsen);

        // button click event
        bAbsen.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // creating new product in background thread
                if(inputNama.getText().toString().trim().equals(""))
                {
                    inputNama.setError("Nama harus diisi!");
                }
                if(inputDivisi.getText().toString().trim().equals(""))
                {
                    inputDivisi.setError("Divisi harus diisi!");
                }
                if(inputKeterangan.getText().toString().trim().equals(""))
                {
                    inputKeterangan.setError("Keterangan harus diisi!");
                }
                else{
                    new doAbsen().execute();
                }
            }
        });
    }

    /**
     * Background Async Task to Create new product
     * */
    class doAbsen extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(NewProductActivity.this);
            progressDialog.setMessage("Proses Data Absensi.....");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
            progressDialog.show();
        }

        /**
         * Creating product
         * */
        protected String doInBackground(String... args) {
            String nama = inputNama.getText().toString();
            String divisi = inputDivisi.getText().toString();
            String keterangan = inputKeterangan.getText().toString();

            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("nama", nama));
            params.add(new BasicNameValuePair("divisi", divisi));
            params.add(new BasicNameValuePair("keterangan", keterangan));

            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_create_product,
                    "POST", params);


            Intent i = new Intent(NewProductActivity.this, AllProductsActivity.class);
            finish();
            startActivity(i);
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * **/
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            progressDialog.dismiss();
        }

    }
}